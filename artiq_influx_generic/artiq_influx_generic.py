#!/usr/bin/env python3
import argparse
import asyncio
import atexit
import json
import logging
import time
from collections import namedtuple

import aiohttp
from artiq_comtools.artiq_influxdb_schedule import format_influxdb
from sipyco import common_args
from sipyco.asyncio_tools import atexit_register_coroutine
from sipyco.asyncio_tools import TaskObject
from sipyco.pc_rpc import Server

logger = logging.getLogger(__name__)


# A tuple to be stored in the queue
InfluxQItem = namedtuple("InfluxQItem", ("fields", "tags", "timestamp"))


def get_argparser():
    parser = argparse.ArgumentParser(
        description="InfluxDB logger for generic data",
        epilog="RPC interface to InfluxDB with support for generic "
        "data logging. Unlike artiq_influx or artiq_influx_schedule, this "
        "controller does not Subscribe to any Publishers, and only writes "
        "data when requested over the RPC interface. ",
    )
    group = parser.add_argument_group("database")
    group.add_argument(
        "--baseurl-db",
        default="http://localhost:8086",
        help="base URL to access InfluxDB (default: %(default)s)",
    )
    group.add_argument("--user-db", default="", help="InfluxDB username")
    group.add_argument("--password-db", default="", help="InfluxDB password")
    group.add_argument("--database", default="db", help="database name to use")
    group.add_argument("--table", default="generic", help="table name to use")
    common_args.simple_network_args(parser, [("control", "control", 3276)])
    common_args.verbosity_args(parser)
    return parser


class DBWriter(TaskObject):
    def __init__(
        self, base_url, user, password, database, table, queue_size=1000, batch_size=500
    ):
        self.base_url = base_url
        self.user = user
        self.password = password
        self.database = database
        self.table = table
        self.batch_size = batch_size

        self._queue = asyncio.Queue(queue_size)

    async def update(self, fields, tags, timestamp=None):
        """
        Add an entry to the selected table / database in InfluxDB

        :param fields: dict of fields to be stored
        :param tags: dict of tags to store
        :param timestamp: Timestamp of entry: float of seconds since unix epoch
        :raise TimeoutError: Raised on timeout of communication with database
        """
        if not timestamp:
            timestamp = time.time()

        if not self.task:
            # Task hasn't yet been started. Fine, add to the queue in preparation
            pass
        elif self.task.done():
            # The _do() loop isn't running! This entry will never be written
            raise RuntimeError("artiq_influx_generic: _do() loop is not running")

        item = InfluxQItem(fields, tags, timestamp)

        try:
            self._queue.put_nowait(item)
        except asyncio.QueueFull:
            logger.warning(
                "failed to update generic influx entry asynchronously: "
                "too many pending updates. Waiting..."
            )
            try:
                QUEUE_TIMEOUT = 1
                await asyncio.wait_for(self._queue.put(item), timeout=QUEUE_TIMEOUT)
            except asyncio.TimeoutError:
                logger.exception(
                    "failed to make space in queue after {}s".format(QUEUE_TIMEOUT),
                    exc_info=True,
                )
                raise TimeoutError(
                    "artiq_influx_generic: failed to make space in queue after {}s".format(
                        QUEUE_TIMEOUT
                    )
                )

    async def wait_queue(self, timeout=None):
        """
        Wait for the write queue to be completely processed.

        :param timeout: Max time in s to wait. Forever if None
        :raise TimeoutError: Raised if timeout reached
        """
        try:
            await asyncio.wait_for(self._queue.join(), timeout=timeout)
        except asyncio.TimeoutError:
            # Convert asyncio.TimeoutError to a builtin so that
            # the RPC exception packer can handle it
            raise TimeoutError

    def _to_write_str(self, tags, fields, timestamp):
        tags = ",".join(
            "{}={}".format(k, format_influxdb(v, tag=True)) for (k, v) in tags.items()
        )
        fields = ",".join(
            "{}={}".format(k, format_influxdb(v, tag=False))
            for (k, v) in fields.items()
        )
        timestamp_influx = int(round(timestamp * 1e3))
        data = "{},{} {} {}".format(self.table, tags, fields, timestamp_influx)
        return data

    async def _do(self):
        async with aiohttp.ClientSession() as session:
            while True:
                url = self.base_url + "/write"
                params = {
                    "u": self.user,
                    "p": self.password,
                    "db": self.database,
                    "precision": "ms",
                }

                # Wait for data to write
                queue_items = [await self._queue.get()]

                logger.debug("Data received: {}".format(queue_items))

                try:
                    # Is there more in the queue? If so, read it all out and write everything together
                    while (
                        self._queue.qsize() > 0 and len(queue_items) <= self.batch_size
                    ):
                        queue_items.append(await self._queue.get())

                    logger.debug("Data for write: {}".format(queue_items))

                    # Process into strings
                    data_strs = [
                        self._to_write_str(q.tags, q.fields, q.timestamp)
                        for q in queue_items
                    ]

                    # Merge strings
                    data_str = "\n".join(data_strs)

                    logger.debug(
                        "(q = {}) Writing data: {}".format(
                            self._queue.qsize(), data_str
                        )
                    )
                    try:
                        response = await session.post(url, params=params, data=data_str)
                        logger.debug(
                            "(q = {}) write complete".format(self._queue.qsize())
                        )
                    except Exception:
                        logger.error(
                            "got exception trying to write to database", exc_info=True
                        )
                    else:
                        if response.status not in (200, 204):
                            content = (await response.content.read()).decode().strip()
                            logger.warning(
                                "got HTTP status %d " "trying to write to database: %s",
                                response.status,
                                content,
                            )
                        response.close()

                finally:
                    for _ in queue_items:
                        self._queue.task_done()


class DBReader:
    def __init__(self, base_url, user, password, database, table):
        self.base_url = base_url
        self.user = user
        self.password = password
        self.database = database
        self.table = table
        self.query_timeout = 2.0

    async def read(self, values, filter_tag, filter_tag_val):
        """
        Submit a query to influxdb and return the resultant parsed JSON answer.
        :param values: List of values to query
        :param filter_tag: single tag to filter by
        :param filter_tag_val: value of given tag to filter for
        :return: tuple of lists of timestamp and values
        """
        q = 'SELECT "{values}" FROM "{table}" WHERE "{tag_name}"=\'{tag_val}\''.format(
            table=self.table,
            tag_name=filter_tag,
            tag_val=filter_tag_val,
            values='","'.join(values),
        )
        logger.debug('Sending query "%s"', q)
        result = await self.query(q)

        # Extract the field and timestamps from the result
        try:
            series = result["results"][0]["series"][0]["values"]
            t, x = list(zip(*series))
            t = list(t)
            x = list(x)

            return t, x
        except KeyError:
            raise KeyError('Query "{}" returned no results'.format(q))

    async def query(self, query):
        """
        Send a query to the InfluxDB database and return the response.

        Wait for a maximum of self.query_timeout seconds for the reply.
        :param query: Valid influxdb query string.
        :return: dict of response from influxdb
        """

        return await asyncio.wait_for(
            self.send_query(query), timeout=self.query_timeout
        )

    async def send_query(self, query):
        url = self.base_url + "/query"
        params = {
            "u": self.user,
            "p": self.password,
            "db": self.database,
            "precision": "ms",
            "q": query,
        }

        logger.debug('Sending query "{}"'.format(query))

        async with aiohttp.ClientSession() as session:
            try:
                response = await session.get(url, params=params)
                logger.debug('Query complete. Response: "{}"'.format(response))
            except Exception:
                logger.error(
                    "Got exception trying to read from database", exc_info=True
                )
                raise
            else:
                if response.status == 200:
                    content = (await response.content.read()).decode().strip()
                    logger.debug(
                        "got HTTP status %d " "reading from database: %s",
                        response.status,
                        content,
                    )
                else:
                    # Don't log this error on the controller: the influxdb instance responded to us fine,
                    # so the problem lies with the query. The controller did its job: the error is at the client end.
                    # This exception will be sent back to the client over RPC.
                    raise ValueError(
                        "Unexpected HTTP status in InfluxDB response: {}".format(
                            response
                        )
                    )
            finally:
                response.close()

            decoded = json.loads(content)

            return decoded


class InfluxController:
    def __init__(self, db_writer: DBWriter, db_reader: DBReader):
        self._db_writer = db_writer
        self._db_reader = db_reader

    def ping(self):
        """
        Ping the controller, always returning True
        :return: True
        """
        return True

    async def write(self, fields, tags, timestamp=None):
        """
        Write a timestamped entry to the selected InfluxDB database and measurement.

        :param fields:  Dict of field name -> value
        :param tags:    Dict of tag name -> value
        :param time:    Timestamp of this measurement (float in s since UNIX epoch).
                        Default: None uses time of this call
        :return: None
        """
        logger.debug("Submitting log to DBWriter queue")
        await self._db_writer.update(fields=fields, tags=tags, timestamp=timestamp)

    async def wait_writes(self, timeout=None):
        """
        Wait until all pending writes have completed.

        This controller maintains a queue of results to be written to the database to avoid blocking. If
        you need to ensure that all data has been written before you continue, call this function: it will block until
        writing is completed.

        :param timeout: Max time in s to wait. Forever if None
        :raise TimeoutError: Raised if timeout reached
        """

        await self._db_writer.wait_queue(timeout=timeout)

    async def read(self, values, filter_tag, filter_tag_val):
        """
        Submit a query to influxdb and return the resultant parsed JSON answer.
        :param values: List of values to query
        :param filter_tag: single tag to filter by
        :param filter_tag_val: value of given tag to filter for
        :return: tuple of lists of timestamp and values
        """
        return await self._db_reader.read(values, filter_tag, filter_tag_val)

    async def query(self, query):
        """
        Submit an arbitrary query to influxdb and return the parsed result
        :param query: A valid InfluxDB query
        :return: dict
        """
        return await self._db_reader.query(query)


def start_and_restart(obj: TaskObject, max_retries=None):
    """
    Start a TaskObject, and restart it if it fails

    :param obj: TaskObject to be started
    :param max_retries: Maximum number of times to restart the Task
    """

    async def starter(attempt_no):
        try:
            # Start the Task
            obj.start()
            # Extract the _do() loop Future and wait for its completion
            await obj.task
        except asyncio.CancelledError:
            raise
        except Exception:
            # If it completed for any reason other than a cancellation, restart it
            if attempt_no < max_retries:
                logger.exception(
                    "Restarting artiq_influx_generic after exception", exc_info=True
                )
                await starter(attempt_no + 1)
            else:
                logger.exception(
                    "Max number of retries reached: exiting", exc_info=True
                )

    asyncio.ensure_future(starter(attempt_no=0))


def main(extra_args=None):
    args = get_argparser().parse_args(extra_args)
    common_args.init_logger_from_args(args)

    logger.debug("Launching artiq_influx_generic controller")

    loop = asyncio.get_event_loop()
    atexit.register(loop.close)

    writer = DBWriter(
        args.baseurl_db, args.user_db, args.password_db, args.database, args.table
    )
    start_and_restart(writer, max_retries=5)

    atexit_register_coroutine(writer.stop)

    reader = DBReader(
        args.baseurl_db, args.user_db, args.password_db, args.database, args.table
    )

    server = InfluxController(writer, reader)
    rpc_server = Server({"influx_logger": server}, builtin_terminate=True)
    loop.run_until_complete(
        rpc_server.start(common_args.bind_address_from_args(args), args.port_control)
    )
    atexit_register_coroutine(rpc_server.stop)

    loop.run_until_complete(rpc_server.wait_terminate())


if __name__ == "__main__":
    main()
