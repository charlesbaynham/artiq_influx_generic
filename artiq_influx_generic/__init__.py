"""artiq_influx_generic

RPC interface to InfluxDB with support for generic
data logging. Unlike artiq_influx or artiq_influx_schedule, this controller does
not Subscribe to any Publishers, and only writes data when requested over the
RPC interface.
"""
from .artiq_influx_generic import InfluxController
from .artiq_influx_generic import main

__author__ = "Charles Baynham <charles.baynham@gmail.com>"
__all__ = ["InfluxController", "main"]
__version__ = "0.2.2"
