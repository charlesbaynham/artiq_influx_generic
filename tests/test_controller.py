import logging
import os
from random import randint
from time import sleep
from time import time

import numpy as np
import pytest
from sipyco.pc_rpc import Client

from artiq_influx_generic import main as artiq_influx_main

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


DATABASE_NAME = "testing"
TABLE_NAME = "test_table"
CONTROLLER_PORT = "3276"
TAG_NAME = "test_type"


@pytest.fixture
def client(server):
    """
    Get a client to the running server and reset the device before and after usage

    Runs for every usage
    """
    c = get_new_client()
    yield c
    c.close_rpc()


@pytest.fixture(scope="module")
def server():
    """
    Start a server and return an RPC client pointing at the server

    Runs once per module
    """

    import asyncio
    import threading

    # Start the RPC server in another thread
    def wrapper():
        try:
            asyncio.get_event_loop()
        except RuntimeError:
            asyncio.set_event_loop(asyncio.new_event_loop())

        extra_args = [
            "--database",
            DATABASE_NAME,
            "--table",
            TABLE_NAME,
            "--port",
            CONTROLLER_PORT,
            # Explicitly bind to localhost to avoid IPv6 default which is not
            # supported on Gitlab CI
            "--bind",
            "127.0.0.1",
            # Override the default behaviour which is to add the IPv6 localhost
            # in addition to the main bind address
            "--no-localhost-bind",
        ]

        if "INFLUXDB_URL" in os.environ:
            print(
                f"Using environmental variable for URL:  INFLUXDB_URL = {os.environ['INFLUXDB_URL']}"
            )
            extra_args += ["--baseurl", os.environ["INFLUXDB_URL"]]

        artiq_influx_main(extra_args=extra_args)

    t = threading.Thread(target=wrapper, name="server_thread")
    t.start()

    sleep(3)

    client = get_new_client()

    # Create a new database called "testing"
    client.query("CREATE DATABASE testing")

    yield

    # Fixure complete. Kill the server
    client.terminate()
    t.join()
    logger.debug("Done")


def get_new_client():
    """
    Gets a client for the artiq_influx_generic controller.
    """
    return Client(host="127.0.0.1", port=CONTROLLER_PORT)


def test_on_path():
    import subprocess as sp

    r = sp.run(["artiq_influx_generic", "--help"], check=True)


class TestInfluxController:
    def test_rpc_list(self, client):
        c = client

        print(c.get_rpc_method_list())

    def test_ping(self, client):
        assert client.ping()

    def test_write(self, client):
        client.write(fields={"val": 123}, tags={TAG_NAME: "hello"})

    def test_read_single(self, client):
        random_tag_value = "read_test_{}".format(randint(0, 9999999))
        val_in = randint(0, 999999)

        client.write(fields={"val": val_in}, tags={TAG_NAME: random_tag_value})

        client.wait_writes(timeout=5)

        t, val_out = client.read(
            values=["val"], filter_tag=TAG_NAME, filter_tag_val=random_tag_value
        )
        logger.debug("Results are (%s, %s)", t, val_out)

        assert 1 == len(val_out)
        assert val_in == val_out[0]

    def test_read_multiple(self, client):
        random_tag_value = "read_test_{}".format(randint(0, 9999999))
        N = 10
        times_in = time() + 1e-3 * np.array(range(0, N))
        # Different times to avoid collisions
        vals_in = [randint(0, 999999) for _ in range(0, N)]

        for t, v in zip(times_in, vals_in):
            client.write(
                fields={"val": v}, tags={TAG_NAME: random_tag_value}, timestamp=t
            )

        client.wait_writes(timeout=5)

        t, vals_out = client.read(
            values=["val"], filter_tag=TAG_NAME, filter_tag_val=random_tag_value
        )
        logger.debug("Results are (%s, %s)", t, vals_out)

        assert vals_in == vals_out

    def test_read_nonexistent(self, client):
        random_tag_value = "read_test_{}".format(randint(0, 9999999))

        with pytest.raises(KeyError):
            t, val_out = client.read(
                values=["val"], filter_tag=TAG_NAME, filter_tag_val=random_tag_value
            )

    def test_query_invalid(self, client):
        with pytest.raises(ValueError):
            client.query("hocus pocus, this won't work")

    def test_query_valid(self, client):
        client.query('SELECT * FROM "{}"'.format(TABLE_NAME))

    def test_slow_writes(self, client):
        # 5000 is the maximum throughput for a "low" load according to
        # https://docs.influxdata.com/influxdb/v1.7/guides/hardware_sizing/
        writes_per_s = 50
        time_to_test = 3  # seconds

        self.write_speed(client, writes_per_s, time_to_test)

    def test_medium_writes(self, client):
        # 5000 is the maximum throughput for a "low" load according to
        # https://docs.influxdata.com/influxdb/v1.7/guides/hardware_sizing/
        writes_per_s = 500
        time_to_test = 3  # seconds

        self.write_speed(client, writes_per_s, time_to_test)

    def write_speed(self, client, writes_per_s, time_to_test):
        random_tag_value = "fast_write_test_{}".format(randint(0, 9999999))

        delay_time = 1.0 / writes_per_s
        n = int(np.ceil(time_to_test / delay_time))

        logger.debug(
            "Testing write throughput for {}s with {} points per second, {} points total".format(
                time_to_test, writes_per_s, n
            )
        )

        t_in = time() + 0.001 * np.array(range(0, n))
        random_data = np.random.rand(n)

        t_start = time()

        t_next = t_start + delay_time

        for t, x in zip(t_in, random_data):
            client.write(
                tags={TAG_NAME: random_tag_value},
                fields={"high_speed_val": x},
                timestamp=t,
            )

            dt = t_next - time()
            if dt > 0:
                sleep(dt)
            t_next += delay_time

        client.wait_writes(timeout=10)

        t_end = time()

        t_out, val_out = client.read(
            values=["high_speed_val"],
            filter_tag=TAG_NAME,
            filter_tag_val=random_tag_value,
        )

        assert len(random_data) == len(
            val_out
        ), "Not all writes ended up in the database"

        for a, b in zip(random_data, val_out):
            assert pytest.approx(a) == pytest.approx(b)

        assert (
            t_end - t_start
        ) <= time_to_test * 1.1, "Writes took more than 10% too long. Final throughput was {}/s".format(
            n / (t_end - t_start)
        )

    def test_fastest_writes(self, client):
        # Write as fast as possible then calculate the throughput we got
        num_points = 5000

        random_tag_value = "fast_write_test_{}".format(randint(0, 9999999))

        # Prepare some data
        t_in = time() + 0.001 * np.array(range(0, num_points))
        random_data = np.random.rand(num_points)

        # Make sure it's a fair test
        client.wait_writes()

        t_start = time()

        for t, x in zip(t_in, random_data):
            client.write(
                tags={TAG_NAME: random_tag_value},
                fields={"high_speed_val": x},
                timestamp=t,
            )

        client.wait_writes(timeout=10)

        t_end = time()

        t_out, val_out = client.read(
            values=["high_speed_val"],
            filter_tag=TAG_NAME,
            filter_tag_val=random_tag_value,
        )

        assert len(random_data) == len(
            val_out
        ), "Not all writes ended up in the database"

        for a, b in zip(random_data, val_out):
            assert pytest.approx(a) == pytest.approx(b)

        rate = num_points / (t_end - t_start)
        logger.info("Average throughput: {}/s".format(rate))

        assert rate >= 500, "Rate < 500/s. Achieved rate was {}/s".format(rate)


def main():
    # logger.setLevel(logging.DEBUG)
    logger.info("Running unit tests")

    pytest.main()


if __name__ == "__main__":
    main()
