Summary
=======

RPC interface to InfluxDB with support for generic data logging. Unlike
artiq_influx or artiq_influx_schedule, this controller does not Subscribe to any
Publishers, and only writes data when requested over the RPC interface.

This driver allows you to quickly write data to an influx database. It is
designed to write to a single table, although support for multiple tables would
be an easy extension. Writing supports any combination of tags and fields.

This driver is implemented as a network controller so that it can queue data to
be written asyncronously. The package includes unit tests that will fail if
write performance drops below 500 writes/s - on my PC the driver can support
>3500 writes/s with buffering and batching handled transparently by the driver.
This allows it to achieve much higher performance than the official python API
client provided by influxdb.

The driver also supports reading from the database, however queries are very
basic, supporting only one tag filter. For more advanced querying, I'd recommend
the official client instead.

Installation
============

Poetry
------

This project is packaged by [Poetry](https://python-poetry.org/), an excellent
package manager for python. If you use poetry, you can install this project using::

    poetry add https://gitlab.com/charlesbaynham/artiq_influx_generic.git

Poetry also integrates well with Nix and ARTIQ via the
[poetry2nix](https://github.com/nix-community/poetry2nix) project.

Pip
---

This package cannot currently by installed from PyPI since it depends on
``sipyco`` and ``artiq_comtools``, which have not been uploaded. To install it, run::

    pip install git+https://gitlab.com/charlesbaynham/artiq_influx_generic.git

Nix
---

This is a normal python package, and so can be installed using Nix like any
other python package. I recommend using
[poetry2nix](https://github.com/nix-community/poetry2nix). If you do this,
simply install using poetry as described above, i.e.::

    poetry add https://gitlab.com/charlesbaynham/artiq_influx_generic.git

However, if you prefer to use raw Nix, you can build this repository directly
using e.g.::

    {
        inputs.artiq_influx_generic.url = "git+https://gitlab.com/charlesbaynham/artiq_influx_generic.git";
        inputs.artiq_influx_generic.flake = false;

        inputs.artiq-comtools.url = github:m-labs/artiq-comtools;
        inputs.artiq-comtools.inputs.nixpkgs.follows = "nixpkgs";

        inputs.sipyco.url = github:m-labs/sipyco;
        inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

        outputs = { self, nixpkgs, flake-utils, artiq_influx_generic, artiq-comtools, sipyco }:
            flake-utils.lib.eachDefaultSystem (system:
                let
                    pkgs = nixpkgs.legacyPackages.${system};

                    artiq_influx_generic_package = pkgs.python3Packages.buildPythonPackage {
                        name = "artiq_influx_generic";
                        src = artiq_influx_generic;
                        format = "pyproject";
                        propagatedBuildInputs = with pkgs.python3Packages; [
                            # Required for building the package
                            poetry-core

                            # Python dependencies
                            numpy
                            aiohttp

                            # Flake dependencies
                            artiq-comtools.packages.${system}.default
                            sipyco.packages.${system}.default
                        ];
                    };
                in
                    {...}
            );
    }


Example code
============

A simple example usage is::

    class Test(EnvExperiment):
        def build(self):
            self.setattr_device("influx_logger")

        def run(self):
            self.influx_logger.write(tags={"my_tag": "hello"}, fields={"val": 123}, timestamp=time.time())

            t, val_out = self.influx_logger.read(
                values=["val"], filter_tag="my_tag", filter_tag_val="hello"
            )

For this to work, the controller must be running. This is best achieved by adding it to your artiq device_db like so::

    "influx_logger": {
        "type": "controller",
        "host": "::1",
        "port": "3276",
        "target": "influx_logger",
        "command": "artiq_influx_generic --port {port} --bind {bind}",
    }


Development
===========

For developing the package, install poetry through your method of choice then clone this repository::

    git clone https://gitlab.com/charlesbaynham/artiq_influx_generic.git

And install the dependencies::

    poetry install

If you want automatic style normalization of your code, execute::

    pre-commit install

From now on, whenever you commit, ``pre-commit`` will check your code for
styling errors. If ``pre-commit`` finds any errors it will correct them, but
the commit will be aborted. This is so that you can check its work before you
continue. If you're happy, just repeat the commit. If you change your mind
about this feature, run ``pre-commit uninstall``.

Testing
=======

To run the unit tests, you'll need a local instance of influxdb running. To start one, the easiest way is to use docker::

    docker run -p 8086:8086 --rm --name=influxdb -e INFLUXDB_HTTP_AUTH_ENABLED=false influxdb:1.8

You can then run the unit tests using::

    pytest

To test maximum throughput on your machine, run::

    pytest -k fastest -s


Authors
=======

`artiq_influx_generic` was written by `Charles Baynham <charles.baynham@gmail.com>`_.
